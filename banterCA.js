//Game created by Kieran Hoey. Copyright 2016.
var gameBanter = function()
{
  this.canvas = document.getElementById('canvas_2D'),
  this.context = this.canvas.getContext('2d'),
  this.fpsElement = document.getElementById('fps'),
  this.gameCommenced = false,
  this.scoreElement = document.getElementById('score'),

  //Directions
  this.LEFT = 1,
  this.RIGHT = 2,

  //Constants 
  this.PLATFORM_HEIGHT = 8,
  this.PLATFORM_STROKE_WIDTH = 2,
  this.PLATFORM_STROKE_STYLE = 'rgb(0,0,0)', //black
  this.SHORT_DELAY = 50,

  this.CHARACTER_LEFT = 50,
  this.STARTING_RUNNER_TRACK = 1,
  this.TRANSPARENT = 0,
  this.OPAQUE = 1.0,

  //Score
  this.score = 0;

  //Track baselines
  this.TRACK_1_BASELINE = 500,
  this.TRACK_2_BASELINE = 400,
  this.TRACK_3_BASELINE = 300,
  this.TRACK_4_BASELINE = 200,
  this.TRACK_5_BASELINE = 100,

  //Scrolling constants
  this.BACKGROUND_VELOCITY = 42,
  this.STARTING_BACKGROUND_VELOCITY = 0,
  this.STARTING_BACKGROUND_OFFSET = 0,
  this.STARTING_PLATFORM_OFFSET = 0,
  this.PLATFORM_VELOCITY_MULTIPLIER = 4,

  //Images
   this.spritesheet = new Image(),
   this.bgSpritesheet = new Image(),
   this.enemySprite = new Image(),
   this.spriteOffset = 0,
   this.lastAdvanceTime = 0,
   this.RUN_ANIMATION_RATE = 10,

   //Background width and height
   this.BACKGROUND_WIDTH = 1000,
   this.BACKGROUND_HEIGHT = 500,

  //Loading screen
  this.loadingElement = document.getElementById('loading'),
  this.loadingTitleElement = document.getElementById('loading-title'),

  //Toast
  this.toastElement = document.getElementById('toast'),

  //Instructions
  this.instructionsElement = document.getElementById('instructions'),

  //Copyright
  this.copyrightElement = document.getElementById('copyright'),

  //Sound and music
  this.soundAndMusicElement = document.getElementById('sound-and-music'),

  //States
  this.paused = false,
  this.PAUSED_CHECK_INTERVAL = 200,
  this.windowHasFocus = true,
  this.countdownInProgress = false,

  //Time
  this.lastAnimationFrameTime = 0,
  this.fps = 60,
  this.bgVelocity = this.STARTING_BACKGROUND_VELOCITY,
  this.backgroundOffset = this.STARTING_BACKGROUND_OFFSET,
  this.platformOffset = this.STARTING_PLATFORM_OFFSET,
  this.lastFpsUpdateTime = 0,
  this.platformVelocity,
  this.pauseStartTime,

  this.characterCellsFront = [
  {left:14, top:9, width:99, height:115},
  {left:130, top:9, width:99, height:115},
  {left:249, top:9, width:99, height:115},
  ];

  this.characterCellsLeft = [
  {left:19, top:149, width:86, height:111},
  {left:139, top:149, width:86, height:111},
  {left:259, top:149, width:86, height:111},
  ];

  this.characterCellsBack = [
  {left:14, top:284, width:91, height:106},
  ];

  this.characterCellsRight = [
  {left:14, top:409, width:86, height:111},
  {left:129, top:409, width:86, height:111},
  {left:254, top:409, width:86, height:111},
  ];

  this.characterCellsFrontWalk = [
  {left:14, top:529, width:91, height:121},
  {left:134, top:529, width:91, height:121},
  {left:254, top:529, width:91, height:121},
  {left:374, top:529, width:91, height:121},
  {left:494, top:529, width:91, height:121},
  {left:614, top:529, width:91, height:121},
  {left:734, top:529, width:91, height:121},
  {left:854, top:529, width:91, height:121},
  {left:974, top:529, width:91, height:121},
  {left:1094, top:529, width:91, height:121},
  ];

  this.characterCellsLeftWalk = [
  {left:4, top:664, width:116, height:116},
  {left:129, top:664, width:96, height:116},
  {left:244, top:664, width:96, height:116},
  {left:369, top:664, width:96, height:116},
  {left:489, top:664, width:101, height:116},
  {left:604, top:664, width:116, height:116},
  {left:729, top:664, width:96, height:116},
  {left:849, top:664, width:91, height:116},
  {left:974, top:664, width:96, height:116},
  {left:1089, top:664, width:101, height:116},
  ];

  this.characterCellsBackWalk = [
  {left:14, top:780, width:91, height:124},
  {left:134, top:780, width:91, height:124},
  {left:254, top:780, width:91, height:124},
  {left:374, top:780, width:91, height:124},
  {left:494, top:780, width:91, height:124},
  {left:614, top:780, width:91, height:124},
  {left:734, top:780, width:91, height:124},
  {left:854, top:780, width:91, height:124},
  {left:974, top:780, width:91, height:124},
  {left:1094, top:780, width:91, height:124},
  ];

  this.characterCellsRightWalk = [
  {left:4, top:924, width:101, height:116},
  {left:134, top:924, width:96, height:116},
  {left:254, top:924, width:96, height:116},
  {left:369, top:924, width:96, height:116},
  {left:479, top:924, width:116, height:116},
  {left:609, top:924, width:101, height:116},
  {left:729, top:924, width:96, height:116},
  {left:859, top:924, width:96, height:116},
  {left:979, top:924, width:96, height:116},
  {left:1079, top:924, width:116, height:116},
  ];

  this.enemyCell = [
  {left:41, top:223, width:91, height:132},
  {left:257, top:223, width:99, height:115},
  {left:461, top:223, width:99, height:115},
  ];

  this.enemyData = [
       {left: 1000, top: this.TRACK_5_BASELINE},
       {left: 1300, top: this.TRACK_3_BASELINE},
       {left: 1600, top: this.TRACK_5_BASELINE},
       {left: 1900, top: this.TRACK_3_BASELINE},
       {left: 2200, top: this.TRACK_5_BASELINE},
       {left: 2500, top: this.TRACK_5_BASELINE},
       {left: 2800, top: this.TRACK_3_BASELINE},
       {left: 3100, top: this.TRACK_3_BASELINE},
       {left: 3400, top: this.TRACK_5_BASELINE},
       {left: 3700, top: this.TRACK_5_BASELINE},
       {left: 4000, top: this.TRACK_3_BASELINE},
       {left: 4300, top: this.TRACK_5_BASELINE},
       {left: 4600, top: this.TRACK_3_BASELINE},
       {left: 4900, top: this.TRACK_5_BASELINE},
       {left: 5200, top: this.TRACK_5_BASELINE},
       {left: 5500, top: this.TRACK_3_BASELINE},
       {left: 5800, top: this.TRACK_3_BASELINE},
       {left: 6100, top: this.TRACK_5_BASELINE},
       {left: 6400, top: this.TRACK_5_BASELINE},
       {left: 6700, top: this.TRACK_3_BASELINE},
       {left: 7000, top: this.TRACK_5_BASELINE},
       {left: 7300, top: this.TRACK_3_BASELINE},
       {left: 7600, top: this.TRACK_5_BASELINE},
       {left: 7900, top: this.TRACK_5_BASELINE},
       {left: 8200, top: this.TRACK_3_BASELINE},
       {left: 8500, top: this.TRACK_3_BASELINE},
       {left: 8800, top: this.TRACK_5_BASELINE},
       {left: 9100, top: this.TRACK_5_BASELINE},
       {left: 9400, top: this.TRACK_3_BASELINE},
       {left: 9700, top: this.TRACK_5_BASELINE},
       {left: 10000, top: this.TRACK_3_BASELINE},
       {left: 10300, top: this.TRACK_5_BASELINE},
       {left: 10600, top: this.TRACK_5_BASELINE},
       {left: 10900, top: this.TRACK_3_BASELINE},
       {left: 11200, top: this.TRACK_3_BASELINE},
       {left: 11500, top: this.TRACK_5_BASELINE},
       {left: 11800, top: this.TRACK_5_BASELINE},
       {left: 12100, top: this.TRACK_3_BASELINE},
       {left: 12400, top: this.TRACK_5_BASELINE},
       {left: 12700, top: this.TRACK_3_BASELINE},
       {left: 13000, top: this.TRACK_5_BASELINE},
       {left: 13300, top: this.TRACK_5_BASELINE},
       {left: 13600, top: this.TRACK_3_BASELINE},
       {left: 13900, top: this.TRACK_3_BASELINE},
       {left: 14200, top: this.TRACK_5_BASELINE},
       {left: 14500, top: this.TRACK_5_BASELINE},
       {left: 14800, top: this.TRACK_3_BASELINE},
       {left: 15100, top: this.TRACK_5_BASELINE},
       {left: 15400, top: this.TRACK_3_BASELINE},
       {left: 15700, top: this.TRACK_5_BASELINE},
       {left: 16000, top: this.TRACK_5_BASELINE},
       {left: 16300, top: this.TRACK_3_BASELINE},
       {left: 16600, top: this.TRACK_3_BASELINE},
       {left: 16900, top: this.TRACK_5_BASELINE},
      
   ];

// Platforms

   this.platformData = [
      // Screen 1
      {
         left:      10,
         width:     17000,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(150,190,255)',
         opacity:   0,
         track:     1,
         pulsate:   false,
      }
   ];

this.platforms = [];  
this.enemies = [];

this.sprites = [];

this.platformArtist = 
  {
    draw: function(sprite, context)
    {
      var PLATFORM_STROKE_WIDTH = 1.0,
        PLATFORM_STROKE_STYLE = 'black',
        top;
      top = gameBanter.calculatePlatformTop(sprite.track);
      context.lineWidth = PLATFORM_STROKE_WIDTH;
      context.strokeStyle = PLATFORM_STROKE_STYLE;
      context.fillStyle = sprite.fillStyle;

      context.strokeRect(sprite.left, top, sprite.width, 
      sprite.height);
      context.fillRect(sprite.left, top, sprite.width, 
      sprite.height);
    }
  };

   //Sprite behaviours
   this.runBehaviour = 
   {
      lastAdvanceTime: 0,
      execute: function(sprite, now, fps, context, 
            lastAnimationFrameTime)
      {
         if(sprite.runAnimationRate === 0)
         {
            return;
         }
         if(this.lastAdvanceTime === 0)
         {
            this.lastAdvanceTime = now;
         }
         else if(now-this.lastAdvanceTime > 
            1000/sprite.runAnimationRate)
         {
            sprite.artist.advance();
            this.lastAdvanceTime = now;
         }
      }
   };

   //Pacing on platforms
   this.paceBehaviour = 
   {
    setDirection: function(sprite)
    {
      var sRight = sprite.left + sprite.width,
          pRight = sprite.platform.left + sprite.platform.width;

      if(sprite.direction === undefined)
      {
        sprite.direction = gameBanter.RIGHT;
      }
      if(sRight > pRight && sprite.direction === gameBanter.RIGHT)
      {
        sprite.direction = gameBanter.LEFT;
      }
      else if(sprite.left < sprite.platform.left && sprite.direction === gameBanter.LEFT)
      {
        sprite.direction = gameBanter.RIGHT;
      }
    },

    setPosition: function(sprite, now, lastAnimationFrameTime)
    {
      var pixelsToMove = sprite.velocityX * (now - lastAnimationFrameTime)/1000;

      if (sprite.direction === gameBanter.RIGHT)
      {
        sprite.left += pixelsToMove;
      }
      else
      {
        sprite.left -= pixelsToMove;
      }
    },

    execute: function(sprite, now, fps, context, lastAnimationFrameTime)
    {
      this.setDirection(sprite);
      this.setPosition(sprite, now, lastAnimationFrameTime);
    }
   };
};

gameBanter.prototype = {

	createSprites: function()
   {
      this.createPlatformSprites(),
      this.createEnemySprites(),
      this.createCharacterSprite(),

      this.initializeSprites();
      this.addSpritesToSpriteArray();
   },

   positionSprites: function (sprites, spriteData) {
      var sprite;

      for (var i = 0; i < sprites.length; ++i) {
         sprite = sprites[i];

         if (spriteData[i].platformIndex) { 
            this.putSpriteOnPlatform(sprite,
               this.platforms[spriteData[i].platformIndex]);
         }
         else {
            sprite.top  = spriteData[i].top;
            sprite.left = spriteData[i].left;
         }
      }
   },
   

   addSpritesToSpriteArray: function()
   {
    for(var i=0; i< this.platforms.length; ++i)
      {
         this.sprites.push(this.platforms[i]);
      }

    for(var i=0; i< this.enemies.length; ++i)
      {
         this.sprites.push(this.enemies[i]);
      }

      this.sprites.push(this.character);
   },

  initializeImages: function()
  {
    this.bgSpritesheet.src = 'images/backgroundBanter.png';
    this.spritesheet.src = 'images/banterSpriteSheet.png';
    this.enemySprite.src = 'images/enemyBanterSprite.png';
    

    this.spritesheet.onload = function (e)
    {
      gameBanter.backgroundLoaded();
    };
  },

  backgroundLoaded: function()
  {
    var LOADIND_SCREEN_TRANSITION_DURATION = 2000;
    this.fadeOutElements(this.loadingElement, LOADIND_SCREEN_TRANSITION_DURATION);

    setTimeout( function(){
      gameBanter.startGame();
      gameBanter.gameCommenced = true;
    }, LOADIND_SCREEN_TRANSITION_DURATION);
  },

  initializeSprites: function() 
  {
    this.positionSprites(this.enemies,  this.enemyData);
  },

  loadingAnimationLoaded: function()
  {
    if(!this.gameCommenced)
    {
      this.fadeInElements(this.loadingTitleElement);
    }
  },

  dimControls: function ()
  {
    FINAL_OPACITY = 0.5;
    gameBanter.instructionsElement.style.opacity = FINAL_OPACITY;
    gameBanter.soundAndMusicElement.style.opacity = FINAL_OPACITY;
  },

  revealCanvas: function ()
  {
    this.fadeInElements(this.canvas);
  },

  revealTopChrome: function()
  {
    this.fadeInElements(this.fpsElement, this.scoreElement);
  },

  revealTopChromeDimmed: function ()
  {
    var DIM = 0.25;

    this.scoreElement.style.display = 'block';
    this.fpsElement.style.display = 'block';

    setTimeout(function ()
    {
      gameBanter.scoreElement.style.opacity = DIM;
      gameBanter.fpsElement.style.opacity = DIM;
    }, this.SHORT_DELAY);
  },

  revealBottomChrome: function()
  {
    this.fadeInElements(this.soundAndMusicElement, 
      this.instructionsElement, this.copyrightElement);
  },

  revealGame: function()
  {
    var DIM_CONTROLS_DELAY = 5000;

    this.revealTopChromeDimmed();
    this.revealCanvas();
    this.revealBottomChrome();

    setTimeout(function ()
    {
      gameBanter.dimControls();
      gameBanter.revealTopChrome();
    }, DIM_CONTROLS_DELAY);
  },

  revealInitialToast: function()
  {
    var INITIAL_TOAST_DURATION = 1500,
    INITIAL_TOAST_DELAY = 3000;

    setTimeout(function()
    {
      gameBanter.revealToast('Banter Central ', INITIAL_TOAST_DELAY);
    }, INITIAL_TOAST_DURATION);
  },


  startGame: function()
  {
    this.revealGame();
    this.revealInitialToast();
    window.requestAnimationFrame(this.animate);
  },

  animate: function (now)
  {
    if(gameBanter.paused)
    {
      setTimeout(function ()
      {
        requestAnimationFrame(gameBanter.animate);
      }, gameBanter.PAUSED_CHECK_INTERVAL);
    }
    else
    {
      gameBanter.fps = gameBanter.calculateFps(now);
      gameBanter.draw(now);
      gameBanter.lastAnimationFrameTime = now;
      requestAnimationFrame(gameBanter.animate);
    } 
  },

  draw: function (now)
  {
    this.setPlatformVelocity();
    this.setOffsets(now);
    this.drawBackground();
    this.updateSprites(now);
    this.drawSprites();
  },

  setPlatformVelocity: function ()
  {
    if(this.scoreElement.innerHTML >= 10 && this.scoreElement.innerHTML <20)
    {
      this.platformVelocity = this.bgVelocity * 
      (this.PLATFORM_VELOCITY_MULTIPLIER*2);
    }
    else if(this.scoreElement.innerHTML >= 20)
    {
      this.platformVelocity = this.bgVelocity * 
      (this.PLATFORM_VELOCITY_MULTIPLIER*5);
    }
    else
    {
      this.platformVelocity = this.bgVelocity * 
      this.PLATFORM_VELOCITY_MULTIPLIER;
    }
  },

  setOffsets: function (now)
  {
    this.setBackgroundOffset(now);
    this.setSpriteOffsets(now);
  },

   setSpriteOffsets: function(now)
   {
      var sprite;
      this.spriteOffset += this.platformVelocity *
         (now-this.lastAnimationFrameTime)/1000;
      for(var i=0; i < this.sprites.length; ++i)
      {
         sprite = this.sprites[i];
         if('character' !== sprite.type)
         {
            sprite.hOffset = this.spriteOffset;
         }
      }
   },

   createPlatformSprites: function()
  {
    var sprite, pd;

    for(var i=0; i< this.platformData.length; ++i)
    {
      pd = this.platformData[i];
      sprite = new Sprite('platform', this.platformArtist);
      sprite.left = pd.left;
      sprite.width = pd.width;
      sprite.height = pd.height;
      sprite.opacity = pd.opacity;
         sprite.fillStyle = pd.fillStyle;
         sprite.track = pd.track;

         sprite.top = this.calculatePlatformTop(pd.track);
         this.platforms.push(sprite);
    }
  },

  calculateFps: function (now)
  {
    var fps = 1/(now - this.lastAnimationFrameTime) * 1000;
    if(now - this.lastFpsUpdateTime > 1000)
    {
      this.lastFpsUpdateTime = now;
      this.fpsElement.innerHTML = fps.toFixed(0) + 'fps';
    }
    return fps;
  },

  setBackgroundOffset: function(now)
  {
    this.backgroundOffset += this.bgVelocity*(now-this.lastAnimationFrameTime)/1000;
    if(this.backgroundOffset < 0 || this.backgroundOffset > this.BACKGROUND_WIDTH)
    {
      this.backgroundOffset = 0;
    }
  },

  turnLeft: function()
  {
    this.bgVelocity = -this.BACKGROUND_VELOCITY;
      this.character.runAnimationRate = this.RUN_ANIMATION_RATE;
      this.character.artist.cells = this.characterCellsLeftWalk;
  },

  turnRight: function()
  {
    this.bgVelocity = this.BACKGROUND_VELOCITY;
      this.character.runAnimationRate = this.RUN_ANIMATION_RATE;
      this.character.artist.cells = this.characterCellsRightWalk;
  },

  jumpUp: function()
  {
    this.character.runAnimationRate = this.RUN_ANIMATION_RATE;
    this.character.artist.cells = this.characterCellsBackWalk;

    if (this.character.track !== 4) 
    {
      this.character.track++;
    }

    this.character.top = gameBanter.calculatePlatformTop(this.character.track) - 124;
  },

  fallDown: function()
  {
    this.character.runAnimationRate = this.RUN_ANIMATION_RATE;
    this.character.artist.cells = this.characterCellsFrontWalk;

    if (this.character.track !== 1) 
    {
      this.character.track--;
    }

    this.character.top = gameBanter.calculatePlatformTop(this.character.track) - 124;
  },

  fadeInElements: function ()
  {
    var args = arguments;
    for(var i=0; i < args.length; ++i)
    {
      console.log(args[i]);
      args[i].style.display = 'block';
    }

    setTimeout(function () {
      for(var i=0; i < args.length; ++i)
      {
        args[i].style.opacity = gameBanter.OPAQUE;
      }
    }, this.SHORT_DELAY);
  },

  fadeOutElements: function ()
  {
    var args = arguments,
      fadeDuration = args[args.length-1];
    for(var i=0; i < args.length-1; ++i)
    {
      console.log(args[i]);
      args[i].style.opacity = gameBanter.TRANSPARENT;
    }

    setTimeout(function () {
    for(var i=0; i < args.length-1; ++i)
    {
      args[i].style.display = 'none';
    }
    }, fadeDuration);
  },

  hideToast: function()
  {
    var TOAST_TRANSITION_DURATION = 450;
    this.fadeOutElements(this.toastElement, TOAST_TRANSITION_DURATION);
  },

  //This should set the text of the toast 
  //and fade it in over the supplied duration
  startToastTransition: function(text, duration){
    this.toastElement.innerHTML = text;
    this.fadeInElements(this.toastElement);
  },

  drawBackground: function ()
  {
    this.context.translate(-this.backgroundOffset, 0);
    //Initially Onscreen
    this.context.drawImage(this.bgSpritesheet, 0, 0,
         this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT, 0, 0, this.BACKGROUND_WIDTH, 
         this.BACKGROUND_HEIGHT);
    //Initially Offscreen
    this.context.drawImage(this.bgSpritesheet, 0, 0,
         this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT, 1000, 0, this.BACKGROUND_WIDTH, 
         this.BACKGROUND_HEIGHT);
    this.context.translate(this.backgroundOffset, 0);
  },

  drawPlatform: function (data)
  {
    var platformTop = this.calculatePlatformTop(data.track);
    this.context.lineWidth = this.PLATFORM_STROKE_WIDTH;
    this.context.strokeStyle = this.PLATFORM_STROKE_STYLE;
    this.context.fillStyle = data.fillStyle;
    this.context.globalAlpha = data.opacity;

    this.context.strokeRect(data.left, platformTop, data.width, 
      this.PLATFORM_HEIGHT);
    this.context.fillRect(data.left, platformTop, data.width, 
      this.PLATFORM_HEIGHT); 
  },

  drawPlatforms: function ()
  {
    var index;
    this.context.translate(-this.platformOffset, 0);
    for(index =0; index < this.platformData.length; ++index)
    {
      this.drawPlatform(this.platformData[index]);
    }
    this.context.translate(this.platformOffset, 0);
  },

  calculatePlatformTop: function(track)
  {
    if(track === 1)
    {
      return this.TRACK_1_BASELINE; 
    }
    else if (track === 2)
    {
      return this.TRACK_2_BASELINE;
    }
    else if(track === 3)
    {
      return this.TRACK_3_BASELINE;
    }
    else if(track === 4)
    {
      return this.TRACK_4_BASELINE;
    }
    else if(track === 5)
    {
      return this.TRACK_5_BASELINE;
    }
  },

  togglePaused: function() {
    var now = +new Date();
    this.paused = !this.paused;
    if(this.paused)
    {
      this.pauseStartTime = now;
    }
    else
    {
      this.lastAnimationFrameTime += (now - this.pauseStartTime);
    }
  },

  revealToast: function(text, duration){
    var DEFAULT_TOAST_DISPLAY_DURATION = 1000;
    duration = duration || DEFAULT_TOAST_DISPLAY_DURATION;

    this.startToastTransition(text, duration);

    setTimeout(function (e)
    {
      
      gameBanter.hideToast();
      
    }, duration);
  },

  //Sprite functions
   createCharacterSprite: function()
   {
      var CHARACTER_LEFT = 50,
          CHARACTER_HEIGHT = 124,
          STARTING_RUNNER_TRACK = 1,
          STARTING_RUN_ANIMATION_RATE = 0;

      this.character = new Sprite('character', new SpriteSheetArtist(
         this.spritesheet, this.characterCellsRight), [this.runBehaviour]);
      this.character.runAnimationRate = STARTING_RUN_ANIMATION_RATE;
      this.character.track = STARTING_RUNNER_TRACK;
      this.character.left = CHARACTER_LEFT;
      this.character.top = this.calculatePlatformTop(this.character.track) - 
      CHARACTER_HEIGHT;
      this.sprites.push(this.character);
   },

   createEnemySprites: function()
   {
      var enemy;
      var ENEMY_CHANGE = 100;
      var ENEMY_CHANGE_INTERVAL= 100;

      for(var i=0; i< this.enemyData.length; ++i)
      {
         enemy = new Sprite('enemy', new SpriteSheetArtist(
            this.enemySprite, this.enemyCell), [new CycleBehaviour(ENEMY_CHANGE, ENEMY_CHANGE_INTERVAL)]);
         enemy.width = this.enemyCell[2].width;
         enemy.height = 99;
        
         this.enemies.push(enemy);
      }
   },

   putSpriteOnPlatform: function(sprite, platformSprite)
   {
      sprite.top = platformSprite.top - sprite.height;
      sprite.left = platformSprite.left;
      sprite.platform = platformSprite;
   },

   isSpriteInView: function(sprite)
   {
      //only draw sprite if visible on canvas
      return sprite.left + sprite.width > sprite.hOffset && 
      sprite.left < sprite.hOffset + this.canvas.width;
   },
   //Update all sprites first before drawing all sprites
   updateSprites: function(now)
   {
      var sprite;
      for(var i=0; i < this.sprites.length; ++i)
      {
         sprite = this.sprites[i];
         if(sprite.visible && this.isSpriteInView(sprite))
         {
            sprite.update(now, this.fps, this.context,
               this.lastAnimationFrameTime);
         }
      }
   },

   demo: function()
   {
      this.score = this.score + 1;
      this.scoreElement.innerHTML = this.score;
    },

   drawSprites: function()
   {
      var sprite;
      for(var i=0; i < this.sprites.length; ++i)
      {
         sprite = this.sprites[i];
         if(sprite.visible && this.isSpriteInView(sprite))
         {
            this.context.translate(-sprite.hOffset,0);
            sprite.draw(this.context);
            this.context.translate(sprite.hOffset,0);
         }
      }
   }
};

//add some vars for keycodes
var KEY_LEFT_ARROW = 37,
KEY_RIGHT_ARROW = 39,
KEY_UP_ARROW =  38,
KEY_DOWN_ARROW = 40,
KEY_S = 83,
KEY_P = 80;

//Add Event Listener
window.addEventListener('keydown', function (e){
  var key = e.keyCode;
  if(key === KEY_LEFT_ARROW)
  {
    gameBanter.turnLeft();
  }
  else if(key === KEY_RIGHT_ARROW)
  {
    gameBanter.turnRight();
  }
  else if(key === KEY_UP_ARROW)
  {
    gameBanter.jumpUp();
  }
  else if(key === KEY_DOWN_ARROW)
  {
    gameBanter.fallDown();
  }
  else if(key === KEY_P)
  {
    gameBanter.togglePaused();
  }
  else if(key === KEY_S)
  {
    gameBanter.demo();
  }
});

window.addEventListener('blur', function (e){
  gameBanter.windowHasFocus = false;
  if(!gameBanter.paused)
  {
    gameBanter.togglePaused();
  }
});

window.addEventListener('focus', function (e){
  var originalFont = gameBanter.toastElement.style.fontSize,
    DIGIT_DISPLAY_DURATION = 1000; //milliseconds
  gameBanter.windowHasFocus = true;
  gameBanter.countdownInProgress = true;
  if(gameBanter.paused)
  {
    gameBanter.toastElement.style.font = '128px Buxton Sketch';
    if(gameBanter.windowHasFocus && gameBanter.countdownInProgress)
    {
      gameBanter.revealToast('3', DIGIT_DISPLAY_DURATION);
    }
    setTimeout(function (e)
    {
      if(gameBanter.windowHasFocus && gameBanter.countdownInProgress)
        gameBanter.revealToast('2', DIGIT_DISPLAY_DURATION);
      
      setTimeout(function (e)
      {
        if(gameBanter.windowHasFocus && gameBanter.countdownInProgress)
          gameBanter.revealToast('1', DIGIT_DISPLAY_DURATION);
      
        setTimeout(function (e)
        {
          if(gameBanter.windowHasFocus && gameBanter.countdownInProgress)
          {
            gameBanter.togglePaused();
          }
          if(gameBanter.windowHasFocus && gameBanter.countdownInProgress)
          {
            gameBanter.toastElement.style.fontSize = originalFont;
          }
          gameBanter.countdownInProgress = false;
        }, DIGIT_DISPLAY_DURATION);
      }, DIGIT_DISPLAY_DURATION);
    }, DIGIT_DISPLAY_DURATION); 
  }
});

var gameBanter = new gameBanter();
gameBanter.initializeImages();
gameBanter.createSprites();